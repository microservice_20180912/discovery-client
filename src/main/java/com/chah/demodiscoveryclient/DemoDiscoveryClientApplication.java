package com.chah.demodiscoveryclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoDiscoveryClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoDiscoveryClientApplication.class, args);
	}
}
